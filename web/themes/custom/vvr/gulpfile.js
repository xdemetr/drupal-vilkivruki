var gulp = require('gulp'),
    gutil = require('gulp-util'),
    ftp = require('vinyl-ftp'),
    sass = require('gulp-sass')(require('sass')),
    autoprefixer = require('gulp-autoprefixer'),
    plumber = require('gulp-plumber'),
    imagemin = require('gulp-imagemin'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify'),
    groupMedia = require('gulp-group-css-media-queries'),
    cleanCSS = require('gulp-clean-css'),
    sassInlineSvg = require('gulp-sass-inline-svg'),
    csso = require('gulp-csso'),

    appSass = 'src/styles/app.scss',
    sassDir = 'src/styles',
    cssDir = 'build/css',

    appImg = 'src/images',
    imgDir = 'build/images',

    jsDir = 'src/js/**/*.js',
    jsDest = 'build/js',

    fontDir = 'src/fonts',
    fontDest = 'src/fonts';

require('dotenv').config();
var localFiles = [
      './build/**/*',
      './templates/**/*',
      './*.html',
      './*.php',
      './*.info',
    ],
    remoteLocation = process.env.REMOTE_LOCATION;

function getFtpConnection() {
  return ftp.create({
    host: process.env.HOST,
    port: process.env.PORT,
    user: process.env.USER,
    password: process.env.PASS,
    parallel: 5,
    log: gutil.log,
  });
}

gulp.task('deploy', function () {
  var conn = getFtpConnection();
  return gulp.src(localFiles, {base: '.', buffer: false})
      .pipe(conn.newer(remoteLocation))
      .pipe(conn.dest(remoteLocation));
});

gulp.task('scripts', function () {
  return gulp.src(jsDir)
      .pipe(concat('app.js'))
      .pipe(gulp.dest(jsDest))
      .pipe(rename('app.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(jsDest));
});

gulp.task('sass', function () {
  return gulp.src(appSass)
      .pipe(plumber())
      .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
      //.pipe(groupMedia())
      .pipe(autoprefixer({browsers: ['last 10 versions', '> 1%']}))
      .pipe(gulp.dest(cssDir))
      .pipe(csso())
      .pipe(gulp.dest(cssDir));
});

gulp.task('sasssvg', function () {
  return gulp.src('build/images/**/*.svg')
      .pipe(sassInlineSvg({
        destDir: 'src/styles/general',
      }));
});

gulp.task('images', function () {
  return gulp.src(appImg + '/**/*.+(png|jpg|gif|svg|jpeg|webp)')
      .pipe(imagemin())
      .pipe(gulp.dest(imgDir));
});

gulp.task('fonts', function () {
  return gulp.src(fontDir + '/**/*.+(woff|woff2)')
      .pipe(gulp.dest(fontDest));
});

gulp.task('css', function () {
  return gulp.src(cssDir + '/*.css')
      .pipe(cleanCSS({compatibility: 'ie8'}))
      .pipe(gulp.dest(cssDir));
});

gulp.task('watch:styles', function () {
  gulp.watch(sassDir, gulp.series('sass'));
});

gulp.task('watch:css', function () {
  gulp.watch(cssDir, gulp.series('minify-css'));
});

gulp.task('watch:scripts', function () {
  gulp.watch(jsDir, gulp.series('scripts'));
});

gulp.task('watch:images', function () {
  gulp.watch(appImg + '/**/*.+(png|jpg|gif|svg)', gulp.series(['images', 'sasssvg']));
});

gulp.task('watch', gulp.series(['sass', 'images', 'scripts'],
    gulp.parallel('watch:styles', 'watch:images', 'watch:scripts'),
));

gulp.task('default', gulp.series(['sass', 'images', 'scripts', 'fonts'],
    gulp.parallel('watch'),
));
