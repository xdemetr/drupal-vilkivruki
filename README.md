# Синхронизация конфигурации

## Экспорт
`drush cex -y`

## Импорт
`drush cim -y`

# Перенос базы

## Импорт базы
`drush sql:cli < dump/dump.sql`

## Экспорт базы
`drush sql-dump > dump/dump.sql`

# Update Drupal
`composer update "drupal/core-*" --with-dependencies`
